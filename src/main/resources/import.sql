# noinspection SqlNoDataSourceInspectionForFile
INSERT INTO USEC_USER (user_id, username, password, enabled, last_password_reset_date) VALUES (1, 'admin', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi',  TRUE , '2015-01-01');
INSERT INTO USEC_USER (user_id, username, password, enabled, last_password_reset_date) VALUES (2, 'usecUser', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC',  FALSE , '2015-01-01');
INSERT INTO USEC_USER (user_id, username, password, enabled, last_password_reset_date) VALUES (3, 'disabled', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', TRUE,'2015-01-01');
INSERT INTO USEC_USER (user_id, username, password, enabled, last_password_reset_date) VALUES (4, 'consumer', '$2a$08$UkVvwpULis18S19S5pZFn.YHPZt3oaqHZnDwqbCW9pft6uFtkXKDC', TRUE,'2015-01-01');

INSERT INTO USEC_AUTHORITY (authority_id, name) VALUES (1, 'ROLE_USER');
INSERT INTO USEC_AUTHORITY (authority_id, name) VALUES (2, 'ROLE_ADMIN');

INSERT INTO USEC_USER_AUTHORITY (user_id, authority_id) VALUES (1, 1);
INSERT INTO USEC_USER_AUTHORITY (user_id, authority_id) VALUES (1, 2);
INSERT INTO USEC_USER_AUTHORITY (user_id, authority_id) VALUES (2, 1);
INSERT INTO USEC_USER_AUTHORITY (user_id, authority_id) VALUES (3, 1);


INSERT INTO USEC_USER_DETAILS (user_id,firstname, lastname, email, user_type) VALUES (1,'admin', 'admin', 'admin@admin.com','A');
INSERT INTO USEC_USER_DETAILS (user_id,firstname, lastname, email, user_type)VALUES (2,'usecUser', 'usecUser', 'enabled@usecUser.com','U');
INSERT INTO USEC_USER_DETAILS (user_id,firstname, lastname, email, user_type) VALUES (3, 'usecUser', 'usecUser', 'disabled@usecUser.com','U');
INSERT INTO USEC_USER_DETAILS (user_id,firstname, lastname, email, user_type) VALUES (4, 'Consumer', 'Consumer', 'Consumer@Consumer.com','U');