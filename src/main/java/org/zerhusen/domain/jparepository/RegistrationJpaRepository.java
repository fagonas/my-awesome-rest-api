package org.zerhusen.domain.jparepository;

import org.springframework.data.repository.CrudRepository;
import org.zerhusen.domain.model.Registration;
import org.zerhusen.security.repository.UsecUserRepository;

/**
 * Created by stephan on 20.03.16.
 */
public interface RegistrationJpaRepository extends CrudRepository<Registration,Long> {
}
