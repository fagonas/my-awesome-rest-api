package org.zerhusen.domain.jparepository;

import org.springframework.data.repository.CrudRepository;
import org.zerhusen.domain.model.Registration;
import org.zerhusen.model.security.UsecUserDetails;

/**
 * Created by stephan on 20.03.16.
 */
public interface UserDetailsJpaRepository extends CrudRepository<UsecUserDetails,Long> {
}
