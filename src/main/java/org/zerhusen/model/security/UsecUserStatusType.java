package org.zerhusen.model.security;

/**
 * Created by Tranb on 11/3/2016.
 */
public enum UsecUserStatusType {
    A("Activate"), P("Pending"), I("DISABLED"), L("LOCKED");
    private  String value;
    UsecUserStatusType(String value) {
        this.value = value;
    }
    public String getValue(){
        return value;
    }
}
