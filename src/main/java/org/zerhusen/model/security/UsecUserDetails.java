package org.zerhusen.model.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.katharsis.resource.annotations.JsonApiId;
import io.katharsis.resource.annotations.JsonApiResource;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Tranb on 11/3/2016.
 */
@Entity
@Table(name = "usec_user_details")
@JsonApiResource(type = "usec_user_details")
public class UsecUserDetails {
    @JsonApiId
    @GenericGenerator(name="generator", strategy ="foreign" , parameters= @org.hibernate.annotations.Parameter(name = "property", value ="usec_user"))
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "firstname", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    private String firstname;

    @Column(name = "lastname", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    private String lastname;

    @Column(name = "email", length = 50)
    @NotNull
    @Size(min = 4, max = 50)
    private String email;


    @Column(name = "user_type", length = 1)
    @NotNull
    private String usecUserType;
    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private UsecUser usecUser;


    public UsecUser getUsecUser(){
        return this.usecUser;
    }

    public void setUsecUser(UsecUser usecUser) {
        this.usecUser = usecUser;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsecUserType() {
        return usecUserType;
    }

    public void setUsecUserType(String usecUserType) {
        this.usecUserType = usecUserType;
    }
}
