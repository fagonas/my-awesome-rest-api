package org.zerhusen.model.security;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN, ROLE_PRODUCER, ROLE_CONSUMER
}