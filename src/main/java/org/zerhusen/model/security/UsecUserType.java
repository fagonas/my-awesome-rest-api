package org.zerhusen.model.security;

/**
 * Created by Tranb on 11/3/2016.
 */
public enum UsecUserType {
    A("ADMIN"),U("USER");
    private  String value;
    UsecUserType(String Value) {
        this.value = value;
    }
    public String getValue(){
        return value;
    }
}