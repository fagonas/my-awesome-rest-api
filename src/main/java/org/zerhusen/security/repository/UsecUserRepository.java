package org.zerhusen.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.zerhusen.model.security.UsecUser;

/**
 * Created by stephan on 20.03.16.
 */
public interface UsecUserRepository extends CrudRepository<UsecUser, Long> {
    UsecUser findByUsername(String username);
}
