package org.zerhusen.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.zerhusen.model.security.UsecUser;
import org.zerhusen.security.JwtUserFactory;
import org.zerhusen.security.repository.UsecUserRepository;

/**
 * Created by stephan on 20.03.16.
 */
@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsecUserRepository usecUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UsecUser usecUser = usecUserRepository.findByUsername(username);

        if (usecUser == null) {
            throw new UsernameNotFoundException(String.format("No usecUser found with username '%s'.", username));
        } else {
            return JwtUserFactory.create(usecUser);
        }
    }
}
