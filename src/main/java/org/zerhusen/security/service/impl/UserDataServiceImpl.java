package org.zerhusen.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.zerhusen.model.security.Authority;
import org.zerhusen.model.security.AuthorityName;
import org.zerhusen.model.security.UsecUser;
import org.zerhusen.model.security.UsecUserDetails;
import org.zerhusen.jsonapi.model.WebUser;
import org.zerhusen.security.repository.UsecUserRepository;
import org.zerhusen.security.service.UserDataService;
import org.zerhusen.utils.Constant;

import java.util.ArrayList;

/**
 * Created by stephan on 20.03.16.
 */
@Service
public class UserDataServiceImpl implements UserDataService {

    @Autowired
    private UsecUserRepository userRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    @Override
    public WebUser createWebUser(WebUser user) {
        UsecUser usecUser = new UsecUser();
        usecUser.setUsername(user.getUsername());
        usecUser.setEnabled(false);
        usecUser.setPassword(bCryptPasswordEncoder.encode(usecUser.getPassword()));
        usecUser.setUsecUserDetails(new UsecUserDetails());
        usecUser.getUsecUserDetails().setEmail(user.getEmail());
        usecUser.getUsecUserDetails().setFirstname(user.getFirstname());
        usecUser.getUsecUserDetails().setLastname(user.getLastname());
        usecUser.setAuthorities(new ArrayList<Authority>());
        Authority authority = new Authority();
        if(Constant.ADMIN_ROLE.equals(user.getWebUserType().getValue())){
            authority.setName(AuthorityName.ROLE_ADMIN);
        }
        else if (Constant.USER_ROLE.equals(user.getWebUserType().getValue())){
            authority.setName(AuthorityName.ROLE_USER);
        }
        usecUser.getAuthorities().add(authority);
        usecUser.getUsecUserDetails().setUsecUserType(user.getEmail());
        usecUser.setUsername(user.getUsername());
        userRepository.save(usecUser);
        return user;
    }

    @Override
    public WebUser activateWebUser(Integer userId) {
        return null;
    }

    @Override
    public WebUser deactivateWebUser(Integer userId) {
        return null;
    }

    @Override
    public WebUser updateWebUser(WebUser user) {
        return null;
    }

    @Override
    public WebUser changePassword(Integer userId, String rawPassword) {
        return null;
    }
}
