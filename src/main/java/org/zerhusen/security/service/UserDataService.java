package org.zerhusen.security.service;

import org.zerhusen.jsonapi.model.WebUser;

/**
 * Created by Tranb on 11/4/2016.
 */
public interface UserDataService {
    WebUser createWebUser(WebUser user);
    WebUser activateWebUser(Integer userId);
    WebUser deactivateWebUser(Integer userId);
    WebUser updateWebUser(WebUser user);
    WebUser changePassword(Integer userId, String rawPassword);
}
