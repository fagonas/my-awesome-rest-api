package org.zerhusen.security;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.zerhusen.model.security.Authority;
import org.zerhusen.model.security.UsecUser;

public final class JwtUserFactory {

    private JwtUserFactory() {
    }

    public static JwtUser create(UsecUser usecUser) {
        return new JwtUser(
                usecUser.getId(),
                usecUser.getUsername(),
                usecUser.getUsecUserDetails().getFirstname(),
                usecUser.getUsecUserDetails().getLastname(),
                usecUser.getUsecUserDetails().getEmail(),
                usecUser.getPassword(),
                mapToGrantedAuthorities(usecUser.getAuthorities()),
                usecUser.getEnabled(),
                usecUser.getLastPasswordResetDate()
        );
    }

    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Authority> authorities) {
        return authorities.stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getName().name()))
                .collect(Collectors.toList());
    }
}
