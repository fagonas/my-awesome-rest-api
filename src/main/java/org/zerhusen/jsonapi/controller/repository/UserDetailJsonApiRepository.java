package org.zerhusen.jsonapi.controller.repository;

import io.katharsis.queryParams.QueryParams;
import io.katharsis.repository.ResourceRepository;
import io.katharsis.repository.annotations.JsonApiRelationshipRepository;
import io.katharsis.resource.annotations.JsonApiResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zerhusen.domain.jparepository.UserDetailsJpaRepository;
import org.zerhusen.model.security.UsecUser;
import org.zerhusen.model.security.UsecUserDetails;

/**
 * Created by Tranb on 12/8/2016.
 */
@Component
public class UserDetailJsonApiRepository implements ResourceRepository<UsecUserDetails,Long>{
    @Autowired
    UserDetailsJpaRepository userDetailsJpaRepository;
    @Override
    public UsecUserDetails findOne(Long aLong, QueryParams queryParams) {
        return userDetailsJpaRepository.findOne(aLong);
    }

    @Override
    public Iterable<UsecUserDetails> findAll(QueryParams queryParams) {
        return  userDetailsJpaRepository.findAll();
    }

    @Override
    public Iterable<UsecUserDetails> findAll(Iterable<Long> longs, QueryParams queryParams) {
        return  userDetailsJpaRepository.findAll();
    }

    @Override
    public void delete(Long aLong) {
        userDetailsJpaRepository.delete(aLong);
    }

    @Override
    public UsecUserDetails save(UsecUserDetails entity) {
        return userDetailsJpaRepository.save(entity);
    }
}
