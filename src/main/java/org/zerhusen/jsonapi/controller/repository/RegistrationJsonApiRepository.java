package org.zerhusen.jsonapi.controller.repository;

import io.katharsis.queryParams.QueryParams;
import io.katharsis.repository.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zerhusen.domain.jparepository.RegistrationJpaRepository;
import org.zerhusen.domain.model.Registration;
import org.zerhusen.model.security.UsecUser;

/**
 * Created by Tranb on 12/8/2016.
 */
@JsonApiResourceRepository(Registration.class)
@Component
public class RegistrationJsonApiRepository {

    @Autowired
    RegistrationJpaRepository registrationJpaRepository;

    @JsonApiFindOne
    public Registration findOne(Long aLong, QueryParams queryParams) {
        return registrationJpaRepository.findOne(aLong);
    }

    @JsonApiFindAll
    public Iterable<Registration> findAll(QueryParams queryParams) {
        return registrationJpaRepository.findAll();
    }

    @JsonApiFindAllWithIds
    public Iterable<Registration> findAll(Iterable<Long> iterable, QueryParams queryParams) {
        return registrationJpaRepository.findAll();
    }

    @JsonApiDelete
    public void delete(Long aLong) {
        registrationJpaRepository.delete(aLong);
    }

    @JsonApiSave
    public Registration save(Registration s) {
        return registrationJpaRepository.save(s);
    }

}
