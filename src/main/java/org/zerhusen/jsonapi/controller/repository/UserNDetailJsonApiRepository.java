package org.zerhusen.jsonapi.controller.repository;

import io.katharsis.repository.annotations.JsonApiRelationshipRepository;
import org.springframework.stereotype.Component;
import org.zerhusen.model.security.UsecUser;
import org.zerhusen.model.security.UsecUserDetails;

/**
 * Created by Tranb on 12/8/2016.
 */
@JsonApiRelationshipRepository(source = UsecUser.class,target = UsecUserDetails.class)
@Component
public class UserNDetailJsonApiRepository{

}
