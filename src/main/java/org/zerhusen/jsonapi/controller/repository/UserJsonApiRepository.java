package org.zerhusen.jsonapi.controller.repository;

import io.katharsis.queryParams.QueryParams;
import io.katharsis.repository.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zerhusen.model.security.UsecUser;
import org.zerhusen.security.repository.UsecUserRepository;

/**
 * Created by Tranb on 12/8/2016.
 */
@JsonApiResourceRepository(UsecUser.class)
@Component
public class UserJsonApiRepository {

    @Autowired
    UsecUserRepository userRepository;

    @JsonApiFindOne
    public UsecUser findOne(Long aLong, QueryParams queryParams) {
        return userRepository.findOne(aLong);
    }

    @JsonApiFindAll
    public Iterable<UsecUser> findAll(QueryParams queryParams) {
        return userRepository.findAll();
    }

    @JsonApiFindAllWithIds
    public Iterable<UsecUser> findAll(Iterable<Long> iterable, QueryParams queryParams) {
        return userRepository.findAll();
    }

    @JsonApiDelete
    public void delete(Long aLong) {
        userRepository.delete(aLong);
    }

    @JsonApiSave
    public UsecUser save(UsecUser s) {
        return userRepository.save(s);
    }

}
