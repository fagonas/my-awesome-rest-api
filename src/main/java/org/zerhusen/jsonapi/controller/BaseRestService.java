package org.zerhusen.jsonapi.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Tranb on 11/4/2016.
 */
@RestController
@CrossOrigin
@RequestMapping("/json")
public class BaseRestService {
}
