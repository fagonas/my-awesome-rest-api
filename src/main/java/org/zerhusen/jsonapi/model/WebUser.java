package org.zerhusen.jsonapi.model;

import org.zerhusen.utils.Constant;

import java.io.Serializable;

/**
 * Created by Tranb on 11/4/2016.
 */
public class WebUser implements Serializable{
    private String email;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private WebUserType webUserType;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public WebUserType getWebUserType() {
        return webUserType;
    }

    public void setWebUserType(WebUserType webUserType) {
        this.webUserType = webUserType;
    }

    public static enum WebUserType {
        A(Constant.ADMIN_ROLE), U(Constant.USER_ROLE);
        String value;
        WebUserType(String value){
            this.value = value;
        }
        public String getValue() {
            return this.value;
        }

    }
}
