package org.zerhusen.jsonapi.model;

/**
 * Created by Tranb on 11/4/2016.
 */
public class BaseResponse {
    private String errorCode;
    private String errorMessage;
    private String result;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
