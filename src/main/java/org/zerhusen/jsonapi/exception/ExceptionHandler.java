package org.zerhusen.jsonapi.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.zerhusen.jsonapi.model.BaseResponse;

/**
 * Created by Tranb on 11/4/2016.
 */
@ControllerAdvice
public class ExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(ExceptionHandler.class);
    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    public BaseResponse handleException(HttpServletRequest request, Exception ex , HttpServletResponse response) {
        BaseResponse baseResponse = new BaseResponse();
        if(ex instanceof JsonApiException) {
            baseResponse.setErrorCode(((JsonApiException) ex).getErrorCode());
            baseResponse.setErrorMessage(((JsonApiException) ex).getErrorMessage());
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        return baseResponse;
    }
}
